*** Settings ***
Library                   SeleniumLibrary

*** Variables ***
${PAGINATION BAR}         xpath = //*[contains(@class, 'mat-paginator ng-star-inserted')]
${MODAL CONFIRM}          Confirm
${MODAL CLOSE}            Close

*** Keywords ***
#The pagination bar as used on both alerts page and order lines page
Pagination bar is present
    Wait Until Element Is Visible    ${PAGINATION BAR}