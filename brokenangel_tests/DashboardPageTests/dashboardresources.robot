*** Settings ***
Resource                          ../commonresourcesandtests.robot

Library                           SeleniumLibrary

*** Variables ***
${DASHBOARD ORDERS}               xpath = //*[contains(@class, 'order-line-request')]
${CD FILTER}                      id:dashboard-filter-1
${CD MEDIA SECTION}               id:media-type-block-1
${CD CLOSE LANE 1}                xpath = (//*[@id='manage-lane-button'])[1]
${REMOVE BUTTON CD LANE 1}        xpath = (//*[@id='dashboard-remove-order-line-button-1'])[1]
${SELECT ALL CD LANE 1}           xpath = (//*[@class='select-all-button'])[1]
${CD CLOSE LANE 2}                xpath = (//*[@id='manage-lane-button'])[2]
${REMOVE BUTTON CD LANE 2}        xpath = (//*[@id='dashboard-remove-order-line-button-2'])[1]
${SELECT ALL CD LANE 2}           xpath = (//*[@class='select-all-button'])[2]
${USB FILTER}                     id:dashboard-filter-2
${USB MEDIA SECTION}              id:media-type-block-2
${USB CLOSE LANE 1}               xpath = (//*[@id='manage-lane-button'])[3]
${REMOVE BUTTON USB LANE 1}       xpath = (//*[@id='dashboard-remove-order-line-button-1'])[2]
${SELECT ALL USB LANE 1}          xpath = (//*[@class='select-all-button'])[3]
${USB CLOSE LANE 2}               xpath = (//*[@id='manage-lane-button'])[4]
${REMOVE BUTTON USB LANE 2}       xpath = (//*[@id='dashboard-remove-order-line-button-2'])[2]
${SELECT ALL USB LANE 2}          xpath = (//*[@class='select-all-button'])[4]
${BLURAY FILTER}                  id:dashboard-filter-3
${BLURAY MEDIA SECTION}           id:media-type-block-3
${BLURAY CLOSE LANE 1}            xpath = (//*[@id='manage-lane-button'])[5]
${REMOVE BUTTON BLURAY LANE 1}    xpath = (//*[@id='dashboard-remove-order-line-button-1'])[3]
${SELECT ALL BLURAY LANE 1}       xpath = (//*[@class='select-all-button'])[5]
${DVD FILTER}                     id:dashboard-filter-4
${DVD MEDIA SECTION}              id:media-type-block-4
${DVD CLOSE LANE 1}               xpath = (//*[@id='manage-lane-button'])[6]
${REMOVE BUTTON BLURAY LANE 1}    xpath = (//*[@id='dashboard-remove-order-line-button-1'])[4]
${SELECT ALL DVD LANE 1}          xpath = (//*[@class='select-all-button'])[6]

${PROCESSED ORDER}                ABCD1234-1234

*** Keywords ***
Click CD filter option
    Wait Until Element Is Visible    ${CD FILTER}
    Click Element    ${CD FILTER}

Confirm only CD lanes are displayed
    #Checks CD lane exists
    Wait Until Element Is Visible    ${CD MEDIA SECTION}
    #Checks no other lanes are visible (We look for the element with the keyword checking for 'hidden' attribute to be present
    Wait Until Element Is Not Visible    ${USB MEDIA SECTION}
    Wait Until Element Is Not Visible    ${BLURAY MEDIA SECTION}
    Wait Until Element Is Not Visible    ${DVD MEDIA SECTION}
    Capture Page Screenshot    Evidence/DashboardPage/CDLanesOnly.png

Click USB filter option
    Wait Until Element Is Visible    ${USB FILTER}
    Click Element    ${USB FILTER}

Confirm only USB lanes are displayed
    #Checks USB lane exists
    Wait Until Element Is Visible    ${USB MEDIA SECTION}
    #Checks no other lanes are visible (We look for the element with the keyword checking for 'hidden' attribute to be present
    Wait Until Element Is Not Visible    ${CD MEDIA SECTION}
    Wait Until Element Is Not Visible    ${BLURAY MEDIA SECTION}
    Wait Until Element Is Not Visible    ${DVD MEDIA SECTION}
    Capture Page Screenshot    Evidence/DashboardPage/USBLanesOnly.png

Click BLURAY filter option
    Wait Until Element Is Visible    ${BLURAY FILTER}
    Click Element    ${BLURAY FILTER}

Confirm only BLURAY lanes are displayed
    #Checks DVD lane exists
    Wait Until Element Is Visible    ${BLURAY MEDIA SECTION}
    #Checks no other lanes are visible (We look for the element with the keyword checking for 'hidden' attribute to be present
    Wait Until Element Is Not Visible    ${CD MEDIA SECTION}
    Wait Until Element Is Not Visible    ${USB MEDIA SECTION}
    Wait Until Element Is Not Visible    ${DVD MEDIA SECTION}
    Capture Page Screenshot    Evidence/DashboardPage/BLURAYLanesOnly.png

Click DVD filter option
    Wait Until Element Is Visible    ${DVD FILTER}
    Click Element    ${DVD FILTER}

Confirm only DVD lanes are displayed
    #Checks DVD lane exists
    Wait Until Element Is Visible    ${DVD MEDIA SECTION}
    #Checks no other lanes are visible (We look for the element with the keyword checking for 'hidden' attribute to be present
    Wait Until Element Is Not Visible    ${CD MEDIA SECTION}
    Wait Until Element Is Not Visible    ${USB MEDIA SECTION}
    Wait Until Element Is Not Visible    ${BLURAY MEDIA SECTION}
    Capture Page Screenshot    Evidence/DashboardPage/DVDLanesOnly.png

#Expected number of orders present on dashboard


#Expected number of orders in WAITING column


#Expected order lines are present in WAITING column


#Expected number of orders in PROCESSED column


#Expected order lines are present in PROCESSED column


Click Close Lane button
    #Close one of the lanes, in this case the second USB lane
    Click Button    ${USB CLOSE LANE 2}
    Capture Page Screenshot    Evidence/DashboardPage/CloseUSBLane2Modal.png

Confirm lane closure
    Click Button    ${MODAL CONFIRM}
    Sleep    2s
    Click Button    ${MODAL CLOSE}

#Unable to test below due to static data of lowside application
#Confirm expected lane is now closed


Click an order in PROCESSED column
    Wait Until Element Is Visible        ${DASHBOARD ORDERS}
    #Clicks specified order with which we will then attempt to remove from dashboard page
    Click Element    xpath = //*[contains(text(), "${PROCESSED ORDER}")]

Click Remove button
    #Now click remove button for the lane the selected order is in
    Click Button    ${REMOVE BUTTON CD LANE 1}
    #Only have sleep here to check the action is completing, this will be replaced by the below commented out step
    Sleep    10

#Unable to test below due to static data of lowside application
#Confirm order is no longer present on dashboard

Click Select All button for a lane with multiple orders in then PROCESSED column
    Wait Until Element Is Visible        ${DASHBOARD ORDERS}
    #Clicks Select all button for specified lane, will select all orders that are neither:
    #Alerted, Cancelled, Withdrawn
    Click Element    ${SELECT ALL USB LANE 1}
    #Only have sleep here to check the action is completing correctly, this can be removed when below commented out step is written
    Sleep    10

#Unable to test below due to static data of lowside application
#Confirm orders are no longer present on dashboard