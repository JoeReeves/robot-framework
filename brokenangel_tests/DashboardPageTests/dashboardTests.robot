*** Settings ***
Resource          ../browserresources.robot
Resource          ../SplashScreenTests/splashscreenresources.robot
Resource          ./dashboardresources.robot

Test Setup        Go to application

Test Teardown     Close Browser

*** Test Cases ***
User can filter on media item type (CD Only)
    [Tags]    Dashboard
    Click Accept button
    Click CD filter option
    Confirm only CD lanes are displayed

User can filter on media item type (USB Only)
    [Tags]    Dashboard
    Click Accept button
    Click USB filter option
    Confirm only USB lanes are displayed

User can filter on media item type (BLURAY Only)
    [Tags]    Dashboard
    Click Accept button
    Click BLURAY filter option
    Confirm only BLURAY lanes are displayed

User can filter on media item type (DVD Only)
    [Tags]    Dashboard
    Click Accept button
    Click DVD filter option
    Confirm only DVD lanes are displayed

#Expected number of orders on dashboard are present
#    Click Accept button
#    Expected number of orders present on dashboard

#Expected orders in WAITING column are present
#    Click Accept button
#    Expected number of orders in WAITING column
#    Expected order lines are present in WAITING column

#Expected orders in PROCESSED column are present
#    Click Accept button
#    Expected number of orders in PROCESSED column
#    Expected order lines are present in PROCESSED column

User can successfully close a lane
    [Tags]    Dashboard
    Click Accept button
    Click Close Lane button
    Confirm lane closure
    #Not able to code below step as lowside application uses static data here
    #Confirm expected lane is now closed

User can remove a single order
    [Tags]    Dashboard    Regression
    Click Accept button
    Click an order in PROCESSED column
    Click Remove button
    #Not able to code below step as lowside application uses static data here
    #Confirm order is no longer present on dashboard

User can remove multiple orders
    [Tags]    Dashboard    Regression
    Click Accept button
    Click Select All button for a lane with multiple orders in then PROCESSED column
    Click Remove button
    #Not able to code below step as lowside application uses static data here
    #Confirm orders are no longer present on dashboard
