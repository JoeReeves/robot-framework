*** Settings ***
Resource              ../browserresources.robot

Library               SeleniumLibrary

*** Variables ***
${ACCEPT BUTTON}      login
${DECLINE BUTTON}     decline

*** Keywords ***
Click Accept button
    Click Button    ${ACCEPT BUTTON}

Click Reject button
    Click Button    ${DECLINE BUTTON}

Dashboard page should be displayed
    Location Should Be    ${DASHBOARD URL}
    Capture Page Screenshot    Evidence/SplashScreenScenarios/DashboardPagePresent.png

Forbidden page should be displayed
    Location Should Be    ${FORBIDDEN URL}
    Capture Page Screenshot    Evidence/SplashScreenScenarios/ForbiddenPagePresent.png