*** Settings ***
Resource          ../browserresources.robot
Resource          ./splashscreenresources.robot

Test Setup        Go to application

Test Teardown     Close Browser

*** Test Cases ***
Accept splash screen rules
    [Tags]    SplashScreen
    Click Accept button
    Dashboard page should be displayed

Reject splash screen rules
    [Tags]    SplashScreen
    Click Reject button
    Forbidden page should be displayed