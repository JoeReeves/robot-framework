*** Settings ***
Resource          ../browserresources.robot
Resource          ../PageNavigationTests/pagenavigationresources.robot
Resource          ../SplashScreenTests/splashscreenresources.robot
Resource          ./detailsmodalpageresources.robot

Test Setup        Go to application

Test Teardown     Close Browser

*** Test Cases ***
User is able to access details modal via dashboard page
    [Tags]    Details Modal    Regression
    Click Accept button
    Select order on dashboard page and view details modal
    Check modal has correct layout

User is able to access details modal via order lines page
    [Tags]    Details Modal    Regression
    Click Accept button
    Click Order Lines nav button
    Select order on order lines page and view details modal
    Check modal has correct layout

Order has correct details displayed
    [Tags]    Details Modal
    Click Accept button
    Select order on dashboard page and view details modal
    Check order table has correct information
    Check Media Items section has correct details
    Check History tab has correct details

User can perform a suspend
    [Tags]    Details Modal
    Click Accept button
    Select order that is in the WAITING column
    Click Suspend button
    #Unable to test step due to static data of lowside application
    #Confirm order is withdrawn

#Unable to do too many other tests due to the limited functionality of the lowside application