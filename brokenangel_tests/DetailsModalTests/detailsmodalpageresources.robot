*** Settings ***
Resource                                ../commonresourcesandtests.robot
Resource                                ../DashboardPageTests/dashboardresources.robot

Library                                 SeleniumLibrary

*** Variables ***
${REFRESH BUTTON}                       xpath = //*[contains(@id, 'detail-refresh-button')]
${DETAILS MODAL CLOSE BUTTON}           xpath = //*[contains(@id, 'modal-action-close')]
${ORDER ON DASHBOARD}                   WXY12345-2345
${WAITING ORDER}                        DEF12345-2345
${MODAL HEADER}                         xpath = //*[contains(@class, 'order-line-id-header')]
${HEADER ID}                            ${ORDER ON DASHBOARD}
${ORDER TABLE IN MODAL}                 xpath = //*[contains(@class, 'modal-order-line-table')]
${ORDER TABLE MODAL ROW}                xpath = //*[@id= 'detail-summary-row']
${REF VALUE}                            ABC-123
${FROM WEB VALUE}                       14/02/2018
${SHIP DATE VALUE}                      14/03/2018
${PRODUCT VALUE}                        FIFA
${MEDIA VALUE}                          CD
${GAMES VALUE}                          30
${ITEMS VALUE}                          10
${STATUS VALUE}                         Removed from Dashboard
${MEDIA ITEMS TAB}                      xpath = //*[contains(text(), 'Items of Media')]
${MEDIA SETS}                           xpath = //*[contains(@class, 'card ng-star-inserted')]
${HISTORY TAB}                          xpath = //*[contains(text(), 'History')]
${LIST OF HISTORY}                      xpath = //*[@class= 'ng-star-inserted']
${REF XPATH}                            xpath = //*[@class= 'mat-cell cdk-column-Ref mat-column-Ref ng-star-inserted' and text()='${ORDER ON ORDER LINES PAGE}']
${ORDER ON ORDER LINES PAGE}            2
${SUSPEND BUTTON}                       xpath = //*[@id= 'action-suspend']
${CLOSE DETAILS BUTTON}                 xpath = //*[@id= 'confirm-no-button']

*** Keywords ***
Select order on dashboard page and view details modal
    Wait Until Element Is Visible        ${DASHBOARD ORDERS}
    #Opens specified order's detais modal, we click the element to highlight and then RETURN key to open
    Click Element    xpath = //*[contains(text(), "${ORDER ON DASHBOARD}")]
    Press Keys       xpath = //*[contains(text(), "${ORDER ON DASHBOARD}")]    RETURN

Check modal has correct layout
    #Check modal has expected elements
    Element Should Be Visible        ${REFRESH BUTTON}
    Element Should Be Visible        ${DETAILS MODAL CLOSE BUTTON}
    Wait Until Element Is Visible    ${MODAL HEADER}
    Wait Until Element Is Visible    ${ORDER TABLE IN MODAL}
    Wait Until Element Is Visible    ${MEDIA ITEMS TAB}
    Wait Until Element Is Visible    ${HISTORY TAB}
    Capture Page Screenshot    Evidence/DetailsModal/DashboardDetailsModal.png

Select order on order lines page and view details modal
    #Opens specified order's detais modal, we click the element to highlight and then RETURN key to open
    Click Element   ${REF XPATH}
    Press Keys      ${REF XPATH}    RETURN
    Capture Page Screenshot    Evidence/DetailsModal/OrderLinesDetailsModal.png

Check order table has correct information
    Element Should Be Visible      ${ORDER TABLE MODAL ROW}
    #Check only one row is present
    Page Should Contain Element    ${ORDER TABLE MODAL ROW}  limit=1

    #/Check row has the expected values included (I.e expect ID, number of games and so forth
    #Check row contains expected Ref value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${REF VALUE}    "Incorrect Reference Returned"
    #Check row contains expected Web value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${FROM WEB VALUE}    "Incorrect Web Value Returned"
    #Check row contains expected Ship value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${SHIP DATE VALUE}    "Incorrect Ship Date Value Returned"
    #Check row contains expected Product value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${PRODUCT VALUE}    "Incorrect Product Value Returned"
    #Check row contains expected Media value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${MEDIA VALUE}    "Incorrect Media Value Returned
    #Check row contains expected Games value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${GAMES VALUE}    "Incorrect Games Value Returned
    #Check row contains expected Items value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${ITEMS VALUE}    "Incorrect Items Value Returned"
    #Check row contains expected Status value
    Element Should Contain    ${ORDER TABLE MODAL ROW}    ${STATUS VALUE}   "Incorrect Status Value Returned"

Check Media Items section has correct details
    #Check only 3 sets are visible (Limitation with static data)
    Page Should Contain Element    ${MEDIA SETS}    limit=3
    #TODO Figure out way of reading QTY value from specific media row?

Check History tab has correct details
    Click Element    ${HISTORY TAB}
    Page Should Contain Element    ${LIST OF HISTORY}    limit=9
    #TODO Figure out way of reading from specific history row?

Select order that is in the WAITING column
    Wait Until Element Is Visible        ${DASHBOARD ORDERS}
    #Opens detais modal for order in waiting column, we click the element to highlight and then RETURN key to open
    Click Element    xpath = //*[contains(text(), "${WAITING ORDER}")]
    Press Keys       xpath = //*[contains(text(), "${WAITING ORDER}")]    RETURN

Click Suspend button
    Wait Until Element Is Visible    ${SUSPEND BUTTON}
    Click Button    ${SUSPEND BUTTON}
    Capture Page Screenshot    Evidence/DetailsModal/SuspendModal.png
    Click Button    ${MODAL CONFIRM}
    #Below sleep used to get round the modal button being greyed out due to it processing request
    #For whatever reason the 'Wait Until Element Is Enabled' did not work
    Sleep    8s
    Click Button    ${CLOSE DETAILS BUTTON}