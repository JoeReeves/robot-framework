*** Settings ***
Library                SeleniumLibrary

*** Variables ***
${SERVER}              localhost:8643
${BROWSER}             Chrome
${DELAY}               0
${LOGIN URL}           https://${SERVER}/login
${DASHBOARD URL}       https://${SERVER}/dashboard
${FORBIDDEN URL}       https://${SERVER}/forbidden
${ORDERLINES URL}      https://${SERVER}/orderLines
${USBWRITER URL}       https://${SERVER}/mediaWriter
${ALERTSPAGE URL}      https://${SERVER}/alerts
${ERROR URL}           https://${SERVER}/error
${NOT FOUND URL}       https://${SERVER}/pagethatdoestexist

*** Keywords ***
Go to application
    Open Browser               ${LOGIN URL}    ${BROWSER}    options=add_argument("--ignore-certificate-errors")
    Maximize Browser Window
    Set Selenium Speed         ${DELAY}
    Location Should Be         ${LOGIN URL}