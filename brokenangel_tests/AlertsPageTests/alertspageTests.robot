*** Settings ***
Resource          ../browserresources.robot
Resource          ../SplashScreenTests/splashscreenresources.robot
Resource          ../PageNavigationTests/pagenavigationresources.robot
Resource          ./alertspageresources.robot
Resource          ../commonresourcesandtests.robot

Test Setup        Go to application

Test Teardown     Close Browser

*** Test Cases ***
Alerts page layout is correct (Table and pagination present)
    [Tags]    AlertsPage    Regression
    Click Accept button
    Click Alerts nav button
    Expected table is present
    Expected table Columns are present
    Pagination bar is present

Correct number of alerts in table
    [Tags]    AlertsPage
    Click Accept button
    Click Alerts nav button
    Confirm correct number of alerts in table

User can access alert acknowledgement modal
    [Tags]    AlertsPage    Regression
    Click Accept button
    Click Alerts nav button
    Open up alert modal for alert
    Confirm acknowledge alert modal is displayed

User can cancel out of acknowledging an alert
    [Tags]    AlertsPage
    Click Accept button
    Click Alerts nav button
    Open up alert modal for alert
    Click Close button on alert modal
#   Cannot do final step as although we can click the button, as the lowside app uses static data
#   there is nothing to say the acknowledgement has not occured
#   Confirm alert has not been acknowledged

User can acknowledge an alert
    [Tags]    AlertsPage    Regression
    Click Accept button
    Click Alerts nav button
    Open up alert modal for alert
    Click Confirm button on alert modal
#   Cannot do final step as although we can click the button, as the lowside app uses static data
#   there is nothing to say it has actually been acknowledged
#   Confirm alert has now been acknowledged