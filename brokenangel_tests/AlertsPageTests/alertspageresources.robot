*** Settings ***
Resource                        ../commonresourcesandtests.robot

Library                         SeleniumLibrary

*** Variables ***
${ALERT TABLE}                  alerts-table
${ALERT ICON TABLE HEADER}      xpath = //*[contains(@class, 'cdk-column-alertFlag mat-column-alertFlag ng-star-inserted')]
${ORDERLINE TABLE HEADER}       Order Line Reference:
${DATE TABLE HEADER}            Date/Time:
${TYPE TABLE HEADER}            Type:
${MESSAGE TABLE HEADER}         Message:
${ALERT TABLE ROWS}             xpath = //*[contains(@class, 'mat-row')]
#Need to figure out how to call from a csv file an expected order line which we can then replace "CVB12345-6534"
${ALERT TO ACKNOWELDGE}         CVB12345-6534
${ACKNOWLEDGE ALERT MODAL}      xpath = //*[contains(@class, 'acknowledge-alert-header')]

*** Keywords ***
Expected table is present
    Element Should Be Visible        ${ALERT TABLE}

Expected table Columns are present
    #Have to use Wait rather than table as there is not text header for this column. Instead, we are forced to use this keyword
    Wait Until Element Is Visible    ${ALERT ICON TABLE HEADER}
    Table Header Should Contain      ${ALERT TABLE}    ${ORDERLINE TABLE HEADER}
    Table Header Should Contain      ${ALERT TABLE}    ${DATE TABLE HEADER}
    Table Header Should Contain      ${ALERT TABLE}    ${TYPE TABLE HEADER}
    Table Header Should Contain      ${ALERT TABLE}    ${MESSAGE TABLE HEADER}
    Capture Page Screenshot    Evidence/AlertPageScenarios/AlertPageTable.png

Confirm correct number of alerts in table
    Wait Until Element Is Visible    ${ALERT TABLE ROWS}
    Page Should Contain Element      ${ALERT TABLE ROWS}  limit=8

Open up alert modal for alert
    Wait Until Element Is Visible    ${ALERT TABLE ROWS}
    #Auto Clicks first alert icon in table
    #Click Element    xpath = //*[contains(@id, 'new-alerts-notifier')]

    #Opens specified order's alert icon in table, we click the element to highlight and then RETURN key to open
    Click Element    xpath = //*[contains(text(), "${ALERT TO ACKNOWELDGE}")]
    Press Keys       xpath = //*[contains(text(), "${ALERT TO ACKNOWELDGE}")]    RETURN

Confirm acknowledge alert modal is displayed
    Wait Until Element Is Visible    ${ACKNOWLEDGE ALERT MODAL}
    Capture Page Screenshot   Evidence/AlertPageScenarios/AckAlertModal.png

Click Close button on alert modal
    Click Button    ${MODAL CLOSE}
    Capture Page Screenshot   Evidence/AlertPageScenarios/BackToAlertsPage.png

Click Confirm button on alert modal
    Click Button    ${MODAL CONFIRM}
    #Below sleep used to get round the modal button being greyed out due to it processing request
    #For whatever reason the 'Wait Until Element Is Enabled' did not work
    Sleep    8s
    Click Button    ${MODAL CLOSE}

#Unable to write as lowside application will not show any difference
#Confirm alert has not been acknowledged

#Unable to write as lowside application will not show any difference
#Confirm alert has now been acknowledged
