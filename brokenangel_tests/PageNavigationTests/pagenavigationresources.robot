*** Settings ***
Resource                                ../browserresources.robot

Library                                 SeleniumLibrary

*** Variables ***
${ORDER LINES NAV LINK}                 order-lines-nav-link
${USB PRODUCTION NAV LINK}              media-writer-nav-link
${ALERTS NAV LINK}                      alerts-nav-link
${ERROR PAGE TEXT LOCATION}             //p
${PAGE DOES NOT EXIST TEXT LOCATION}    //p
${ERROR PAGE TEXT}                      There has been an error. Please contact support.
${PAGE DOES NOT EXIST TEXT}             The page you are looking for doesn't exist. Are you sure this is where you wanted to go?

*** Keywords ***
Click Order Lines nav button
    Click Element   ${ORDER LINES NAV LINK}

Click USB Production nav button
    Click Element    ${USB PRODUCTION NAV LINK}

Click Alerts nav button
    Click Element    ${ALERTS NAV LINK}

Order Lines page should be displayed
    Location Should Be    ${ORDERLINES URL}
    Capture Page Screenshot    Evidence/PageNavigationScenarios/OrderLinesPagePresent.png

USB Production page should be displayed
    Location Should Be    ${USBWRITER URL}
    Capture Page Screenshot    Evidence/PageNavigationScenarios/USBProductionPagePresent.png

Alerts Production page should be displayed
    Location Should Be    ${ALERTSPAGE URL}
    Capture Page Screenshot    Evidence/PageNavigationScenarios/AlertsPagePresent.png

Go to Error page
    Go To                 ${ERROR URL}

Go to Not Found page
    Go To                 ${NOT FOUND URL}

Correct error text is displayed
    Element Text Should Be    ${ERROR PAGE TEXT LOCATION}    ${ERROR PAGE TEXT}
    Capture Page Screenshot    Evidence/PageNavigationScenarios/ErrorTextDisplayed.png

Correct not found text is displayed
    Element Text Should Be    ${PAGE DOES NOT EXIST TEXT LOCATION}    ${PAGE DOES NOT EXIST TEXT}
    Capture Page Screenshot   Evidence/PageNavigationScenarios/NotFoundTextDisplayed.png