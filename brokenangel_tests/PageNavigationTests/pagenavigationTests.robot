*** Settings ***
Resource          ../browserresources.robot
Resource          ../SplashScreenTests/splashscreenresources.robot
Resource          ./pagenavigationresources.robot

Test Setup        Go to application

Test Teardown     Close Browser

*** Test Cases ***
User can navigate to Order Lines Page using the navigation bar tab
    [Tags]    Navigation    Regression
    Click Accept button
    Click Order Lines nav button
    Order Lines page should be displayed

User can navigate to the USB Production Page using the navigation bar tab
    [Tags]    Navigation    Regression
    Click Accept button
    Click USB Production nav button
    USB Production page should be displayed

User can navigate to the Alerts Page using the navigation bar tab
    [Tags]    Navigation    Regression
    Click Accept button
    Click Alerts nav button
    Alerts Production page should be displayed

Error Page is accessibile with correct text displayed
    [Tags]    Navigation    Regression
    Click Accept button
    Go to Error page
    Correct error text is displayed

Not Found Page is accessible with correct text displayed
     [Tags]    Navigation    Regression
     Click Accept button
     Go to Not Found page
     Correct not found text is displayed