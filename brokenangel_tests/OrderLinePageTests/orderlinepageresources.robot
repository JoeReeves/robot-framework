*** Settings ***

Library                               SeleniumLibrary

*** Variables ***
${ORDERS LINES TABLE}                 order-lines-summary-table
${CANCEL ICON TABLE HEADER}           xpath = //*[contains(@class, 'mat-header-cell cdk-column-Cancelled mat-column-Cancelled')]
${REF TABLE HEADER}                   Ref:
${FROM WEB TABLE HEADER}              From Web:
${SHIP DATE TABLE HEADER}             Ship Date:
${PRODUCT TABLE HEADER}               Product:
${PRODUCT DROPDOWN}                   product-type-dropdown
${PRODUCT TABLE HEADER XPATH}         xpath = //*[contains(@class, 'cdk-column-Product')]
${MEDIA TABLE HEADER}                 Media:
${MEDIA DROPDOWN}                     media-type-dropdown
${ITEMS TABLE HEADER}                 Items:
${STATUS TABLE HEADER}                Status:
${STATUS DROPDOWN}                    status-type-dropdown
${REPO ICON TABLE HEADER}             xpath = //*[contains(@class, 'mat-header-cell cdk-column-Repro mat-column-Repro')]
${REPRO DROPDOWN}                     reproduction-dropdown
${TIME SELECTOR}                      xpath = //*[contains(@id, 'date-dropdown')]
${SEARCH BOX}                         xpath = //*[contains(@id, 'order-line-summary-search')]
${7DAYS ELEMENT}                      Last 7 Days
${30DAYS ELEMENT}                     Last 30 Days
${ORDER TO SEARCH FOR}                123
${ORDER LINE TABLE ROWS}              xpath = //*[contains(@class, 'mat-row')]
${FILTER BY PRODUCT}                  FIFA
${PRODUCT FILTER ORDER}               1
${FILTER BY MEDIA}                    CD
${MEDIA FILTER ORDER 1}               3
${MEDIA FILTER ORDER 2}               1
${FILTER BY STATUS}                   Complete
${STATUS FILTER ORDER}                1
${FILTER BY REPRO}                    Repro Only
${REPRO FILTER ORDER 1}               3
${REPRO FILTER ORDER 2}               2

*** Keywords ***
Expected orders table is present
    Element Should Be Visible        ${ORDERS LINES TABLE}

Expected orders table columns are present
    #Have to use Wait rather than table as there is not text header for this column. Instead, we are forced to use this keyword
    Wait Until Element Is Visible    ${CANCEL ICON TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${REF TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${FROM WEB TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${SHIP DATE TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${PRODUCT TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${MEDIA TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${ITEMS TABLE HEADER}
    Table Header Should Contain      ${ORDERS LINES TABLE}           ${STATUS TABLE HEADER}
    Wait Until Element Is Visible    ${REPO ICON TABLE HEADER}
    Capture Page Screenshot    Evidence/OrderLinesPage/OrderLinesTable.png

Expected time selector is present
    Wait Until Element Is Visible    ${TIME SELECTOR}

Expect search box is present
    Wait Until Element Is Visible    ${SEARCH BOX}

Change the default time selector to 7 Days
    Wait Until Element Is Visible    ${TIME SELECTOR}
    Click Element                    ${TIME SELECTOR}
    Click Button                     ${7DAYS ELEMENT}
    Capture Page Screenshot    Evidence/OrderLinesPage/SwitchTo7Days.png

#Not able to code below step as lowside application uses static data here
#Confirm orders in table are updated to those in last 7 days

Change the default time selector to 30 Days
    Wait Until Element Is Visible    ${TIME SELECTOR}
    Click Element                    ${TIME SELECTOR}
    Click Button                     ${30DAYS ELEMENT}
    Capture Page Screenshot    Evidence/OrderLinesPage/SwitchTo30Days.png

#Not able to code below step as lowside application uses static data here
#Confirm orders in table are updated to those in last 30 days

Search for an order line
    Click Element    ${SEARCH BOX}
    Press Keys       ${SEARCH BOX}    ${ORDER TO SEARCH FOR}    RETURN
    Capture Page Screenshot    Evidence/OrderLinesPage/ReturnedOrder.png

Confirm expected quantity of matching order lines are returned
    #We search for a specific value and so should only be one order returned (Lowside limitation as well)
    Page Should Contain Element    ${ORDER LINE TABLE ROWS}   limit=1

Confirm expected order line is returned
    #Gets order lines table, checks first row contains searched for value
    Table Row Should Contain    ${ORDERS LINES TABLE}    1    ${ORDER TO SEARCH FOR}

Change drop-down for Product Column
    Element Should Be Visible    ${ORDERS LINES TABLE}
    Click Element    ${PRODUCT DROPDOWN}
    Click Button    ${FILTER BY PRODUCT}

Confirm only matching product orders are shown
    #We filter for a specific product and so should only see the one order returned (Lowside limitation as well)
    Page Should Contain Element      ${ORDER LINE TABLE ROWS}    limit=1
    Table Row Should Contain         ${ORDERS LINES TABLE}    1    ${PRODUCT FILTER ORDER}
    Capture Page Screenshot    Evidence/OrderLinesPage/ProductFilterResults.png

Change drop-down for Media Column
    Element Should Be Visible    ${ORDERS LINES TABLE}
    Click Element    ${MEDIA DROPDOWN}
    Click Button    ${FILTER BY MEDIA}

Confirm only matching media orders are shown
    #We filter for a specific media and so should only see two orders returned (Lowside limitation as well)
    Page Should Contain Element      ${ORDER LINE TABLE ROWS}    limit=2
    Table Row Should Contain         ${ORDERS LINES TABLE}    1    ${MEDIA FILTER ORDER 1}
    Table Row Should Contain         ${ORDERS LINES TABLE}    2    ${MEDIA FILTER ORDER 2}
    Capture Page Screenshot    Evidence/OrderLinesPage/MediaFilterResults.png

Change drop-down for Status Column
    Element Should Be Visible    ${ORDERS LINES TABLE}
    Click Element    ${STATUS DROPDOWN}
    Click Button    ${FILTER BY STATUS}

Confirm only matching status orders are shown
    #We filter for a specific status and so should only see the one order returned (Lowside limitation as well)
    Page Should Contain Element      ${ORDER LINE TABLE ROWS}    limit=1
    Table Row Should Contain         ${ORDERS LINES TABLE}    1    ${STATUS FILTER ORDER}
    Capture Page Screenshot    Evidence/OrderLinesPage/StatusFilterResults.png

Change drop-down for Repro Column
    Element Should Be Visible    ${ORDERS LINES TABLE}
    Click Element    ${REPRO DROPDOWN}
    Click Button    ${FILTER BY REPRO}

Confirm only matching repro orders are shown
    Page Should Contain Element      ${ORDER LINE TABLE ROWS}    limit=2
    Table Row Should Contain         ${ORDERS LINES TABLE}    1    ${REPRO FILTER ORDER 1}
    Table Row Should Contain         ${ORDERS LINES TABLE}    2    ${REPRO FILTER ORDER 2}
    Capture Page Screenshot    Evidence/OrderLinesPage/ReproFilterResults.png