*** Settings ***
Resource          ../browserresources.robot
Resource          ../SplashScreenTests/splashscreenresources.robot
Resource          ../PageNavigationTests/pagenavigationresources.robot
Resource          ./orderlinepageresources.robot
Resource          ../commonresourcesandtests.robot


Test Setup        Go to application

Test Teardown     Close Browser

*** Test Cases ***
Order Lines page layout is correct (Table, Time selector, search box and pagination present)
    [Tags]    OrderLines    Regression
    Click Accept button
    Click Order Lines nav button
    Expected orders table is present
    Expected orders table columns are present
    Expected time selector is present
    Expect search box is present
    Pagination bar is present

User can change the time selector (Last 7 Days)
    [Tags]    OrderLines
    Click Accept button
    Click Order Lines nav button
    Change the default time selector to 7 Days
    #Not able to code below step as lowside application uses static data here
    #Confirm orders in table are updated to those in last 7 days

User can change the time selector (Last 30 Days)
    [Tags]    OrderLines
    Click Accept button
    Click Order Lines nav button
    Change the default time selector to 30 Days
    #Not able to code below step as lowside application uses static data here
    #Confirm orders in table are updated to those in last 30 days

User can run search for an order line id
    [Tags]    OrderLines    Regression
    Click Accept button
    Click Order Lines nav button
    Search for an order line
    Confirm expected quantity of matching order lines are returned
    Confirm expected order line is returned

User can filter on Product column
    [Tags]    OrderLines
    Click Accept button
    Click Order Lines nav button
    Change drop-down for Product Column
    Confirm only matching product orders are shown

User can filter on Media column
    [Tags]    OrderLines
    Click Accept button
    Click Order Lines nav button
    Change drop-down for Media Column
    Confirm only matching media orders are shown

User can filter on Status column
    [Tags]    OrderLines
    Click Accept button
    Click Order Lines nav button
    Change drop-down for Status Column
    Confirm only matching status orders are shown

User can filter on Repro column
    [Tags]    OrderLines
    Click Accept button
    Click Order Lines nav button
    Change drop-down for Repro Column
    Confirm only matching repro orders are shown