##########################################
**BROKEN ANGEL- Example E2E tests README**
##########################################

This project provides an example of automation for the BROKEN ANGEL lowside application using the ROBOT Framework with Selenium Library.

I have tried to create tests that cover the majority of functionality that the app has although there are gaps due to:
- The lowside application uses stub data instead of dynamic data meaning actions where things change will not be reflected
- Some functionality is missing due to it having been developed/improved on the highside
- Outstanding work and questions (See Next Steps section)

I have also used this as a learning exercise as Robot is brand new to myself. As a result, we may be able to improve the elements we are picking but this can be done under a general refactor task.



**Setup**
=========
To setup robot framework, you will need to  make sure you have python installed as well some other tools. At the time of writing, these are as follows:
- Python: 3.8.3
- Robot Framework: 3.2.1
- Chromedriver: 83.0.4103.39  (NOTE: This will depend on your version of chrome, so check what version and then download the associated chromedriver version)

We install Chromedriver as the lowside application only work with chrome, in theory we could use a different application and run in a different browser such as firefox

Instructions on how to install these are available online, Note: You may need to add these to the Environment Variables PATH setting

We also utilise the Selenium Library plugin with robot supporting a number of other plugins and libraries. We may need to add further plugins and libraries depending on what we want to do.



**How to run:**
===============
To run ALL tests, simply run the following command in the terminal
robot brokenangel_tests

You can also run a specific tag(s) of tests. As an example running the below will only run tests that have a 'Regression' tag
robot  --include Regression .



**Structure:**
==============
This project has been designed to follow the Page Object Model, where each directory is for a specific page or piece of functionality. Each directory contains both a:
- Resource.robot file
- Test file

The resource file, contains elements such as button id's for that particular page as well as the code to test each step. As an example:

${ACCEPT BUTTON}    login

Click Accept button
Click Button    ${ACCEPT BUTTON}

The test file, contains the actual scenarios that will be run for each page. As an example:
Accept splash screen rules
[Tags]    SplashScreen
Click Accept button
Dashboard page should be displayed

At a top-most level, we also have a browserresource.robot file which contains details around the browser that will be used in the tests as well as arguements. As an example:
- Browser will be chrome
- It goes to maximum size
- Page to be tested is 'localhost:8643'

Note: As we are using self-signed certs we also have to provide an option that overwrites the cert warning page that we are shown when accessing the application

There is also a commonresourcesandtests.robot file, this contains test steps and elements that are used throughout the application and not just on a specific page



**Screenshots**
===============
I have written several tests to take screenshots which in theory we can then supply as test evidence against the functionality being tested. As an example:
'Capture Page Screenshot    Evidence/PageNavigationScenarios/OrderLinesPagePresent.png'

This means that we will create a directory called Evidence (If one doesn't already exist), followed by a directory relating to test fucntonality (I.e. Navigation). We then provide an image name that gives enough information on what the screenshots proves.

Currently, this will overwrite images as they are re-run meaning evidence history will be wiped each run, we can make changes to this if required but I would envisage this working as follows:
1) Test gets run
2) Screenshot gets saved
3) We have these linked to a test management tool (Zephyr, test rail or some other tool)
4) The screenshot is added to the test management tool test as proof

I think this is something we will make changes to depending on what we can and can't do when on site.



**Report**
============
A report is generated whenever tests are executed, this is the default robot template and no configuration has been conducted as it seems sufficient for our needs



**Next Steps**
==============
The following are the next steps that we need to take:
- Check it works on different operating systems (If we can run on MAC, it's a good sign we can run on linux)
- Implement HTTP request work that Jack has worked on, this will enable us to move away from hardcoded variable values
- Implement reading from a csv/xml/some file that we can create onsite with relevant information
- Investigate running remotely, i.e on a VM



**Useful Links**
================
- Robot Framework: https://robotframework.org/
- Python Website: https://www.python.org/
- Selenium Library: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
- Chromedriver Website: https://chromedriver.chromium.org/